# Opening

https://en.wikipedia.org/wiki/OLX

https://www.meetup.com/pro/pydata

- Warsaw

- no Prague

# Keynote: Hacking the Iron Curtain

COmputere BRAsov (1986)

# Visual concept learning from few images

https://pydata.org/berlin2018/schedule/presentation/2/

- few shot learning

- primitives, subparts ...

- verification networks (same-same, )

- meta learning

- neural turing machines (graves et al.)

- memory augmented networks lstm

- matching networks

- optimization as a model

- https://arxiv.org/pdf/1606.04080.pdf

- meta otimization, lstm meta learner,

# Simple diagrams of convoluted neural networks

https://github.com/stared/keras-sequential-ascii

http://colah.github.io/posts/2015-08-Understanding-LSTMs/

http://setosa.io/ev/image-kernels/

# MISSED: Towards automating machine learning: benchmarking tools for hyperparameter tuning

https://pydata.org/berlin2018/schedule/presentation/23/

### beauty versus information value

- graphcore.ai

- https://github.com/mlajtos/moniel

- https://github.com/lutzroeder/Netron

- keras2ascii

- latex + tikz

- why there are not webgl 3d interactive exploratory tools?

- https://github.com/stared

# Unittesting Smart Contracts

@smcaterpillar

https://pydata.org/berlin2018/schedule/presentation/25/

Populus is a smart contract development framework for the Ethereum blockchain

http://populus.readthedocs.io/en/latest/

- ethereum

- block > fact J, fact N, ...

- fact (for btc it's a transaction, etherereum: transaction + bytecode + persistent data)

- .sol

- miners execute code, check ETH transactions ...

- computation is paid by ETH

- the DAO hack 2016, parity funds freeze 2017

- install populus somehow

- `populus init`

- `selfdestruct` feature of contract

- name the compiler!

- web3 library https://github.com/ethereum/web3.js/

unrelated to talk:

https://www.youtube.com/watch?v=zFScws0mb7M&t=

https://steemit.com/steemit/@smcaterpillar/trufflepig-introducing-the-artificial-intelligence-for-content-curation-and-minnow-support

http://pypet.readthedocs.io/en/latest/

> LUNCH

# James Powell -- Let's SQL Like It's 1992!

common table expressions, window functions, full temporality, table inheritance

https://pydata.org/berlin2018/schedule/presentation/71/

- data modelling

- sql, dml, ddl

### modelling

- what the queries will look like?

- constraints `default 0 check (cost >= 0)`

- full temporality -- keep the state of db -- plugin on github adds
 'asof' -- will **warehouse** all stuff automatically

- seems that he was talking about this one:
https://github.com/arkhipov/temporal_tables
and that here is a tutorial:
http://clarkdave.net/2015/02/historical-records-with-postgresql-and-temporal-tables-and-sql-2011/
and that it's possible to install it like `pip install pgxnclient`
and `pgxn install temporal_tables` under good weather conditions.

# Alexander Hendorf -- How I Made My Computer Write it's First Short Story

- youtube: gene kogan picasso's terminal

- pydata london 2017

- egpu.io

- style transfer: arxiv 1508.06576

- pytorch - research friendly, if it's not working check other commits

- `textract`

- the unreasonable effectiveness of recurrent neural network - blogpost

- @botpickard  https://twitter.com/BotPicard

- LJ speech dataset | naturall tts speech synthesis | tacotron | keithito.com

- voice style transfer, google cloud speech api

# self attention

slides: https://www.dropbox.com/s/hri8veio4rep5g4/Self-Attention_for_NLP_by_Ivan_Bilan.pptx

- GRU

- attention mechanism

- only scans a part which is important

- focus on specific words

- context vector

- nmt with or without lstm

- self attention google brain only attention

- transformer

- encoder/decoder

- layer normalization

- position encoding / wave lenght

- add relative context embeddings within the window

- multihead attention

- linear maps for query key value

- ?softmax

- different heads learn different interactions between words

- tensor2tensor, and other libs

- openai

- disan directional self attention

- relation extraction

- github.com/ivan-bilan

# Stephen Enright-Ward -- Solving very simple substitution ciphers algorithmically

https://github.com/enrightward/masc-cracker

- ascii heatmap

- voynich manuscript question

# Keynote: Building in Privacy and Data Protection -- what is demanded by the GDPR?

https://pydata.org/berlin2018/schedule/presentation/78/


# Lightning Talks

- interpretable ml, molnar, open book

- https://github.com/lopusz/awesome-interpretable-machine-learning

- https://github.com/lopusz/pydata-warsaw-2017

- https://github.com/lopusz/awesome-automatic-machine-learning

- https://www.slideshare.net/lopusz/the-five-tribes-of-machine-learning-explainers

- **enboard** for switching conda environment -- https://gitlab.com/takluyver/enboard

- steemit-blockchain

- steemit-python -- `pip install steem` )) text>money @trufflepig // gh smokingcatterpillar

- blockchain: numerai, botanic, ocean protocol, synapse ai

- private computation, zero knowledge

- github.com/dutc/penv

# MISSED:


### Launch Jupyter to the Cloud: an example of using Docker and Terraform

https://github.com/Cheukting/jupyter-cloud-demo

https://github.com/Cheukting/GCP-GPU-Jupyter

https://www.terraform.io

https://docs.google.com/presentation/d/e/2PACX-1vR0K9gtlPRGRIL6isoVWqa7SOr486yn9p_yCfH-ljtgQa2KpN0J03fOJa_jYgjeVwY3uAJe6GgAxez6/pub?start=false&loop=false&delayms=3000&slide=id.gc6f9e470d_0_0

### manifold learning

https://de.slideshare.net/StefanKhn4/manifold-learning-and-data-visualization

Github repo with worked examples https://github.com/cc-skuehn/Manifold_Learning

### A/B Testing-Reinforcement Learning tutorial

https://github.com/kraktos/MAB

### viz story tutorial

https://github.com/uberwach/leveling-up-viz-story

### Five things I learned while prototyping ML papers

https://speakerdeck.com/ellenkoenig/pydata-bln-2018-five-things-i-learned-while-prototyping-ml-papers

### Extending Pandas using Apache Arrow and Numba

https://www.slideshare.net/xhochy/extending-pandas-using-apache-arrow-and-numba