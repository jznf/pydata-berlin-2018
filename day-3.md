> LUNCH

# Emily Gorcenski -- Going Full Stack with Data Science: Using Technical Readiness Level to Guide Data Science Outcomes

- seeking sre http://shop.oreilly.com/product/0636920063964.do

- aws redshift, kafka, chartio, salesforce

- https://chartio.com

- nasa - trl, technology readiness level

- https://www.nasa.gov/pdf/458490main_TRL_Definitions.pdf

- discovery / delivery

- failure is normal, if you are not failing you are not inovating

# DVC -- Dmitry Petrov -- Data versioning in machine learning projects

- iterative.ai

- dvc.org

- iterative/dvc

- `pip install dvc`

- experiment as commit/branch

- large data files

- metrics per experiment  and other features

- `dvc add images.zip`

- `dvc remote add mys3 s3://some_s3_whatever` and `push mys3`

- optimized for large (>100Gb) files

- pipelines: `dvc run -d images.zip -o images unzip -q images.zip`

- `dvc repro`

- metric driven workflow (derived from gitflow)

- experimentation on hdfs,

# Jens Dittrich -- Big Data Systems Performance: The Little Shop of Horrors

- relational algebra

- predicate pushdown

- parquet

- 4Vs http://www.ibmbigdatahub.com/infographic/four-vs-big-data

- spark, mapreduce

- speedup by data management optimization, efficient format, compression, attribute subsets

- daimond.ai

- youtube.com/user/jensdit

# Jeremy Tuloup -- Practical examples of interactive visualizations in JupyterLab with Pixi.js and Jupyter Widgets

https://github.com/jtpio?tab=repositories

https://gist.github.com/jtpio

https://github.com/jtpio/p5-jupyter-notebook

- https://github.com/jtpio/pixijs-jupyter

- pixi.js http://www.pixijs.com

- abstraction of canvas and webgl

- jupyterlabl widget mvc

- ipywidget

- three js ipyvolume

- SimpleShapeModel `widget.ts`

- `ipyutils`

- https://www.youtube.com/watch?v=FytuB8nFHPQ

- jtplo/p5

# Lightning Talks

- genetic algorhithms

- schedule2cal https://github.com/nicoa/showcase

- tartu pydata, get local co-organizer, meetup vs facebook vs nyx?

- spatiotemporal analysis - kernel density


# MISSED:

### Meaningful histogramming with physt

https://janpipek.github.io/pydata2018-berlin/

# Git w/ James Powell

- https://git-scm.com/book/en/v2/Git-Internals-Git-Objects

- commit, tree, blob

- https://git-scm.com/book/en/v2

# How to scare a fish

- https://github.com/awakenting/master-thesis/blob/master/code/pydata_2018_presentation.ipynb