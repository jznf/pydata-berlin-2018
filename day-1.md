# Using GANs -- Andreas Merentitis
https://pydata.org/berlin2018/schedule/presentation/1/

- google colaboratory
- gan-supervised-pydata.ipynb -- no code online?
- amazon sagemaker https://aws.amazon.com/sagemaker/ fully-managed platform that enables developers and data scientists to quickly and easily build, train, and deploy machine learning models at any scale.
- aws lambda and tensorflow



# PyTorch tutorial

https://pydata.org/berlin2018/schedule/presentation/7/

https://github.com/sotte/pytorch_tutorial

- google colaboratory (test: https://colab.research.google.com/drive/1WAdn7BTc2fcLkMZbxpX0x5m4jo-LMgXT)
- tensor - math term for multidimansional array
- supports ipdb debugger -- interactive debugging
- High performance image augmentation with pillow-simd: https://github.com/uploadcare/pillow-simd and http://python-pillow.org/pillow-perf/
- transfer learning in examples: https://github.com/sotte/pytorch_tutorial/blob/master/notebooks/03_transfer_learning.ipynb


```
from torchvision import models
model = models.resnet18(pretrained=True)
print(model)
```

- pre-trained networks
- tensorboardX PyTorch to Tensorboard
- not much production friendly (unlike caffe, tensorflow)

### gpu w/ google colaboratory:

Go to https://colab.research.google.com
- Create a new notebook
- Enable the GPU: "Edit -> Notebook settings -> Hardware accelerator: GPU -> Save"
- Then install pytorch: !pip install torch torchvision

> LUNCH

# Scaling and reproducing deep learning on Kubernetes with Polyaxon

experiment organization and execution platform

https://pydata.org/berlin2018/schedule/presentation/6/

https://github.com/mouradmourafiq

artinii => https://github.com/mouradmourafiq/philo2vec

https://github.com/polyaxon/polyaxon

https://docs.polyaxon.com/experimentation/concepts/

- install with kubectl -- https://docs.polyaxon.com/installation/introduction/

- pip install cli (and helper)

- login with cli

- dashboard

- project, experiment worklfow

- ```polyaxon tensorboard start```

- specifications yaml files

- quick start project

- polyaxon notebook

- polyaxon user activate (github, gitlab, bitbucket)

# text analysis

https://github.com/bhargavvader/personal

# data viz tutorial

https://github.com/uberwach/leveling-up-viz-story

# production ready DS with spotify luigi

https://github.com/crazzle/pydata_berlin_2018

**workflow**

https://pydata.org/berlin2018/schedule/presentation/75/

https://github.com/spotify/luigi

- baseline model (bananas vs lemons -- circle from opencv 95)

- Keras.model.fit_generator()

- deploy:

- paper: https://www.eecs.tufts.edu/~dsculley/papers/ml_test_score.pdf

- integration testing

- hive - sql layer over distributed filesystem

- versioning of targets, ...

- `requires()` method triggers necesary stuff

# conversational ai

https://pydata.org/berlin2018/schedule/presentation/4/

https://github.com/RasaHQ/rasa-workshop-pydata-berlin

https://github.com/RasaHQ/rasa_nlu

https://github.com/RasaHQ/rasa_core

- classification > intent greet, goodbye, request weather

- nlu_md > intent:category + example

- bag of words svm

- intent + entity + state + previous action > next action

# Deploying a machine learning model to the cloud using AWS Lambda

https://pydata.org/berlin2018/schedule/presentation/14/

https://github.com/bweigel/ml_at_awslambda_pydatabln2018

personal note: api gateway gets **jwt** token ask for auth somewhere
 (some lambda or service) -- no f* logic on the f* gateway! no nothing!
 everyone knows it!

