# Notes from PyData Berlin 2018

https://pydata.org/berlin2018/schedule/

https://www.youtube.com/playlist?list=PLGVZCDnMOq0oQh7daBKy1AW5Q34d0LDsC

If there's anyone interested in my chaotically scattered personal notes from PyData Berlin 2018 conference -- sometimes reflecting the important points of the talks and corresponding links to slides, papers, other materials mentioned etc -- here they are.

Mr. lopusz have done similarly here: https://github.com/lopusz/pydata-berlin-2018